# ItemList

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8 as part of an application process to Wallapop. This README document provides information about the development of an application cappable to manage an item list including a set of features such as sorting by different criteria, selecting favourite items and searching/filtering by the items' attributes.

## Problem Solving

The proposed problem consists of loading a list of items provided in a json file, provide the cappability to order them by any of their attributes (except by the image), search specific items by any of their attributes (except by the image) and mark them as favourite. Moreover, the user should be able to view a favourites list on a modal whereby search specific items and remove items from the favourites list.

*  **Project structure:** as a first step, I designed a project structure designed according to the proposed problem:

    - app                              //Root module
        - components
            - fav-dialog               // Dialog that includes the favourite list          
            - item                     // Item card component
            - item-manager             // Main component
            - search                   // Search form component
            - show-favs                // Show favourite items component. Triggers the modal
            - sorting                  // Sorting component. Triggers the sorting                          
        - models                            
            - item.model.ts            // Item data model
            - search-form.model.ts     // Search form  model
        - services
        - item-data.service.ts         // Service that loads the data from the json file
            - search.service.ts        // Service in charge of searching elements given search criteria
    - assets
        - items-data.json              // Data source

*  **Data collection:** the first step of this development was carrying out the data loading by means of the implementation of an HTTP Service (item-data.service.ts). The DataApiService includes a `get` function just like if the data was being provided by an external REST API, yet in this case the service points to the provided json file. 

The original data provided by the json file was loaded as an array of Item objects that will be later ordered by the javascript function `sort()` given a specific attribute as sort criteria. Please note that the performance of this sorting could have been improved by means of the implementation of either quicksort algorithm, with an O(n2) worst case complexity. Finally this was not done due to time restrictions. 

*  **UI Building:** after the data is loaded, the ItemManagerComponenet dynamically renders all the child ItemComponent. This is carried out through the dynamic rendering provided by angular and, specifically through its `ngFor`directive. This way, each ItemComponent receives an Item object as its input parameter. The UI was built using html and scss supported by the components library Angular Material. Moreover, it is important to highlight the use of flex-box display instead of grids.

*  **Unit Testing:** Angular provides a unit test configuration that allows the creation of a TestBedModule that configures and initializes the desired environment for unit tests supported by Karma and Jasmine. This way, whenever a component or a service is created through Angular CLI, it comes along with a component/service-name.spect.ts, where all the component/service tests can be implemented.

In this project, there is a TestBed configured for every component created, confirming a total 22 test cases. Nevertheless, due to time restrictions, 100% coverage was not reached yet there is still a good coverage level. In this section, the most remarkable test suites that were implemented will be described:

    **item-data.service.spec.ts:** This test suite is in charge of testing the ItemDataService, which loads the data through an http get request that points to the provided json file. Thus, two assertions are implemented here: the first one checks the response type and the second one checks that the number of items retrieved is right.

    **search.service.spec.ts:** This test suite is in charge of testing the search service. The only public function this service has is the `search()` function yet there are several scenarios to test. Therefore, a set of five test cases were included in this suite in order to test the different types of search the service is able to carry out.

    **item.component.spec.ts:** This test suite is a bit different to the previous ones. This is because in this case we are not just testing a public function that expects a specific return value. Thus, here the rendering of the component is testing through a set of five test cases and a provided item mock. Therefore, through these five test cases, the suite checks whether the provided item mock's attributes are being properly rendered when the component is initialized.

The whole test suite can be executed by running `ng test` in the terminal. [Karma](https://karma-runner.github.io).

![Image 1](./src/assets/images/ng-test.png)

## Deploying app in localhost

Bear in mind that, before executing the following commands, it is mandatory to have installed node.js and angular CLI:
    -  To install node.js: nodejs.org/en/download
    -  To install angular cli: `npm install -g @angular/cli`

In order to deploy this app in local, the reader should follow the following steps:
        1. Clone the repository in a local folder.
        2. Execute `npm install` in the root route of the project in order to download all the dependencies inside the node_modules folder.
        3. Run de development server with `ng serve` and navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Future improvements

There are several things that were not done due to time restrictions. Here is a list of the top ones:

*  **Redux architecture:** this approach could have been implemented through the library ngRx. In my opinion, the "single source of truth" approach would have been very suitable to the proposed problem.

*  **SCSS Variables management:** the project currently lacks of a color palette and, moreover, it would be interesting to implement a theming solution through several color palettes.

*  **Improve testing coverage** for future developments, more test cases should be implemented following the approach explained above.


## UI Screenshots
![Image 1](./src/assets/images/item-list-desktop-1.png)

![Image 1](./src/assets/images/item-list-desktop-2.png)

![Image 1](./src/assets/images/item-list-desktop-3.png)

![Image 1](./src/assets/images/item-list-mobile-1.png)

![Image 1](./src/assets/images/item-list-mobile-2.png)

![Image 1](./src/assets/images/item-list-mobile-3.png)