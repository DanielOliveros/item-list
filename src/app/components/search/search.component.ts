import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { SearchFormValue } from '../../models/search-form-value.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() searchEvent = new EventEmitter<SearchFormValue>();

  private searchForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
    price: new FormControl('', [Validators.min(0)]),
    email: new FormControl('', [Validators.email]),
  });

  constructor() { }

  ngOnInit() {
  }

  onSearch(): void {
    if (this.searchForm.valid) {
      this.searchEvent.emit(this.searchForm.value);
    }
  }

}
