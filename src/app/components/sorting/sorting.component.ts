import { Component, Output, EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.scss']
})
export class SortingComponent {
  @Output() sortEvent = new EventEmitter<string>();
  constructor() { }

  onOptionSelected(option: MatSelectChange): void {
    this.sortEvent.emit(option.value);
  }

}
