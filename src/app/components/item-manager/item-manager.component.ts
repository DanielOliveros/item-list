import { Component, OnInit } from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { Item } from '../../models/item.model';
import { ItemDataService } from '../../services/item-data.service';
import { SearchFormValue } from 'src/app/models/search-form-value.model';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-item-manager',
  templateUrl: './item-manager.component.html',
  styleUrls: ['./item-manager.component.scss']
})

export class ItemManagerComponent implements OnInit {
  allItems: Item[] = [];
  searchResults: Item[] = [];
  loadedItems: Item[] = [];
  favItems: Item[] = [];
  pageSize = 5;
  pageSizeOptions: number[] = [2, 5, 10, 20];
  pageEvent: PageEvent;

  constructor(
    private itemDataApiService: ItemDataService,
    private searchService: SearchService
  ) { }

  ngOnInit(): void {
    this.itemDataApiService.getAll().subscribe((items: Item[]) => {
      this.allItems = items;
      this.searchResults = this.allItems;
      this.loadFirstPage();
    });
  }

  loadFirstPage(): void {
    this.loadedItems = [];
    for (let i = 0; i < this.pageSize; i++) {
      if (this.searchResults[i]) {
        this.loadedItems.push(this.searchResults[i]);
      }
    }
  }

  onSearch(searchFormValue: SearchFormValue): void {
    this.searchResults = this.searchService.search(searchFormValue, this.allItems);
    this.loadFirstPage();
  }

  onSort(sortBy: string): void {
    this.searchResults = this.searchResults.sort((a, b) => (a[sortBy] > b[sortBy]) ? 1 : -1);
    this.loadFirstPage();
  }

  onSetFavItem(): void {
    this.favItems = this.allItems.filter(item => item.isfav);
  }

  onPageEvent(pageEvent: PageEvent): void {
    this.loadedItems = [];
    const pageFirstIndex = pageEvent.pageIndex * pageEvent.pageSize;
    for (let i = pageFirstIndex; i < pageFirstIndex + pageEvent.pageSize; i++) {
      if (this.searchResults.length > i) {
        this.loadedItems.push(this.searchResults[i]);
      }
    }
  }
}
