import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { ItemManagerComponent } from './item-manager.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SearchComponent } from '../search/search.component';
import { SortingComponent } from '../sorting/sorting.component';
import { ItemComponent } from '../item/item.component';
import { ShowFavsComponent } from '../show-favs/show-favs.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ItemManagerComponent', () => {
  let component: ItemManagerComponent;
  let fixture: ComponentFixture<ItemManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FormsModule,
        MatSelectModule,
        MatCardModule,
        MatDialogModule
      ],
      declarations: [
        ItemManagerComponent,
        SearchComponent,
        SortingComponent,
        ItemComponent,
        ShowFavsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
