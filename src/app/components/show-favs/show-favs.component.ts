import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Item } from 'src/app/models/item.model';
import { FavDialogComponent } from '../fav-dialog/fav-dialog.component';

@Component({
  selector: 'app-show-favs',
  templateUrl: './show-favs.component.html',
  styleUrls: ['./show-favs.component.scss']
})
export class ShowFavsComponent {
  @Input() favItems: Item[];
  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(FavDialogComponent, {
      width: '1500px',
      data: { favItems: this.favItems }
    });
  }

}
