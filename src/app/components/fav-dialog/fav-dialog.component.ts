import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from 'src/app/models/item.model';
import { ShowFavsComponent } from '../show-favs/show-favs.component';
import { FormControl, FormGroup } from '@angular/forms';
import { SearchService } from 'src/app/services/search.service';

export interface DialogData {
  favItems: Item[];
}

@Component({
  selector: 'app-fav-dialog',
  templateUrl: './fav-dialog.component.html',
  styleUrls: ['./fav-dialog.component.scss']
})
export class FavDialogComponent implements OnInit {
  private searchForm = new FormGroup({
    title: new FormControl(''),
  });
  private searchResults: Item[] = [];

  constructor(
    public dialogRef: MatDialogRef<ShowFavsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private searchService: SearchService) {}

  ngOnInit(): void {
    this.searchResults = this.data.favItems;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  removeFav(item: Item, index: number): void {
    item.isfav = false;
    this.searchResults.splice(index, 1);
  }

  onSearch() {
    this.searchResults = this.searchService.search(this.searchForm.value, this.data.favItems);
  }
}
