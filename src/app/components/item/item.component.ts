import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {
  @Input() item: Item;
  @Output() setFav = new EventEmitter<any>();

  constructor() { }

  onSetFav() {
    this.item.isfav = !this.item.isfav;
    this.setFav.emit();
  }
}
