import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ItemComponent } from './item.component';
import { MatCardModule } from '@angular/material/card';
import { Item } from 'src/app/models/item.model';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;
  const itemMock = new Item({
    title: 'iPhone 6S Oro',
    description: 'Vendo un iPhone 6 S color Oro nuevo y sin estrenar.' +
    'Me han dado uno en el trabajo y no necesito el que me compré.' +
    ' En tienda lo encuentras por 749 euros y yo lo vendo por 740.' +
    'Las descripciones las puedes encontrar en la web de apple. Esta libre.',
    price: 740,
    email: 'iphonemail@wallapop.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemComponent ],
      imports: [
        MatCardModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    component.item = itemMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Item component should render input values', () => {
    it('should render the item title', () => {
      const itemEl: HTMLElement = fixture.debugElement.query(
        By.css('mat-card-title')
        ).nativeElement;

      fixture.detectChanges();
      expect(itemEl.textContent).toEqual(itemMock.title);
    });

    it('should render the item price', () => {
      const itemEl: HTMLElement = fixture.debugElement.query(
        By.css('mat-card-subtitle')
        ).nativeElement;

      fixture.detectChanges();
      expect(itemEl.textContent.indexOf(itemMock.price.toString()) !== -1).toBeTruthy();
    });

    it('should render the item email', () => {
      const itemEl: HTMLElement = fixture.debugElement.query(
        By.css('.email-container')
        ).nativeElement;

      fixture.detectChanges();
      expect(itemEl.textContent).toEqual(itemMock.email);
    });

    it('should render the item description', () => {
      const itemEl: HTMLElement = fixture.debugElement.query(
        By.css('mat-card-content p')
        ).nativeElement;

      fixture.detectChanges();
      expect(itemEl.textContent).toEqual(itemMock.description);
    });

    it('should render the item image', () => {
      const itemEl: HTMLElement = fixture.debugElement.query(
        By.css('.img-container')
        ).nativeElement;

      fixture.detectChanges();
      expect(itemEl.querySelector('img').src).toContain(itemMock.image);
    });
  });
});
