import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

import { IItemData, IItem, Item } from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class ItemDataService {
  private itemDataUrl = 'assets/items-data.json';
  constructor(private http: HttpClient) { }

  getAll(): Observable<Item[]> {
    return this.http.get<IItemData>(this.itemDataUrl).pipe(
      map((itemData: IItemData) => {
        const items: Item[] = [];
        itemData.items.forEach((item: IItem) => {
          items.push(new Item({
            title: item.title,
            description: item.description,
            price: + item.price,
            email: item.email,
            image: item.image,
          }));
        });
        return items;
      })
    );
  }
}
