import { Injectable } from '@angular/core';
import { SearchFormValue } from '../models/search-form-value.model';
import { Item } from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() { }

  search(searchFormValue: SearchFormValue, items: Item[]): Item[] {
    let searchResults = [];
    const notEmptyFormFields = this.getNotEmptyFormFields(searchFormValue);
    if (notEmptyFormFields.length) {
      searchResults = this.doSearch(items, searchFormValue, notEmptyFormFields);
    } else {
      searchResults = items;
    }
    return searchResults;
  }

  private getNotEmptyFormFields(searchFormValue: SearchFormValue): string[] {
    const notEmptyFormFields: string[] = [];
    Object.keys(searchFormValue).forEach((formField: string) => {
      if (searchFormValue[formField]) {
        notEmptyFormFields.push(formField);
      }
    });
    return notEmptyFormFields;
  }

  private isValidItem(item: Item, searchFormValue: SearchFormValue, notEmptyFormFields: string[]): boolean {
    let isValidItem = false;
    notEmptyFormFields.forEach((formField: string) => {
      if (formField === 'description' || formField === 'title') {
        isValidItem = item[formField].toLowerCase().indexOf(searchFormValue[formField].toLowerCase()) !== -1;
      } else {
        isValidItem = item[formField] === searchFormValue[formField];
      }
    });
    return isValidItem;
  }

  private doSearch(items: Item[], searchFormValue: SearchFormValue, notEmptyFormFields: string[]): Item[] {
    const searchResults = [];
    items.forEach((item: Item) => {
      if (this.isValidItem(item, searchFormValue, notEmptyFormFields)) {
        searchResults.push(item);
      }
    });
    return searchResults;
  }
}
