import { TestBed, inject } from '@angular/core/testing';

import { ItemDataService } from './item-data.service';
import { HttpClientModule } from '@angular/common/http';
import { Item } from '../models/item.model';

describe('ItemDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
    providers: [
      ItemDataService
    ]
  }));

  it('should be created', () => {
    const service: ItemDataService = TestBed.get(ItemDataService);
    expect(service).toBeTruthy();
  });

  describe('function getAll', () => {
    it('should return an Observable<Item[]>', () => {
      inject([ItemDataService], (itemDataService: ItemDataService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          expect(Array.isArray(items)).toBe(true);
        });
      });
    });
    it('should return an Observable<Item[]>  with a list of 20 items', () => {
      inject([ItemDataService], (itemDataService: ItemDataService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          expect(items.length).toEqual(20);
        });
      });
    });
  });
});
