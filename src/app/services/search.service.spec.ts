import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { SearchService } from './search.service';
import { ItemDataService } from './item-data.service';
import { Item } from '../models/item.model';
import { SearchFormValue } from '../models/search-form-value.model';


describe('SearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
    providers: [
      SearchService,
      ItemDataService
    ],
  }));

  it('should be created', () => {
    const searchService: SearchService = TestBed.get(SearchService);
    expect(searchService).toBeTruthy();
  });

  describe('function search', () => {
    it('should retrieve all the results when an empty search is done', () => {
      inject([ItemDataService, SearchService],
        (itemDataService: ItemDataService, searchService: SearchService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          const searchFormValue: SearchFormValue = {
            title: '',
            description: '',
            email: '',
            price: '',
          };
          const searchResults: Item [] = searchService.search(searchFormValue, items);
          expect(searchResults.length).toEqual(20);
        });
      });
    });
    it('should search by title', () => {
      inject([ItemDataService, SearchService],
        (itemDataService: ItemDataService, searchService: SearchService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          const searchFormValue: SearchFormValue = {
            title: 'iPhone',
            description: '',
            email: '',
            price: '',
          };
          const searchResults: Item [] = searchService.search(searchFormValue, items);
          expect(searchResults.length).toEqual(1);
          expect(searchResults[0].title).toEqual(searchFormValue.title);
        });
      });
    });
    it('should search by description', () => {
      inject([ItemDataService, SearchService],
        (itemDataService: ItemDataService, searchService: SearchService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          const searchFormValue: SearchFormValue = {
            title: '',
            description: 'Vendo',
            email: '',
            price: '',
          };
          const searchResults: Item [] = searchService.search(searchFormValue, items);
          expect(searchResults.length).toEqual(8);
        });
      });
    });
    it('should search by email', () => {
      inject([ItemDataService, SearchService],
        (itemDataService: ItemDataService, searchService: SearchService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          const searchFormValue: SearchFormValue = {
            title: '',
            description: '',
            email: 'cameramail@wallapop.com',
            price: '',
          };
          const searchResults: Item [] = searchService.search(searchFormValue, items);
          expect(searchResults.length).toEqual(1);
          expect(searchResults[0].email).toEqual(searchFormValue.email);
        });
      });
    });
    it('should search by price', () => {
      inject([ItemDataService, SearchService],
        (itemDataService: ItemDataService, searchService: SearchService) => {
        itemDataService.getAll().subscribe((items: Item[]) => {
          const searchFormValue: SearchFormValue = {
            title: '',
            description: '',
            email: '',
            price: '250',
          };
          const searchResults: Item [] = searchService.search(searchFormValue, items);
          expect(searchResults.length).toEqual(2);
          expect(searchResults[0].price).toEqual(+searchFormValue.price);
        });
      });
    });
  });

});
