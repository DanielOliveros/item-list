import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemManagerComponent } from './components/item-manager/item-manager.component';

const routes: Routes = [
  {
    path: 'item-manager',
    component: ItemManagerComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'item-manager'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
