export interface IItem {
    title: string;
    description: string;
    price: number;
    email: string;
    image: string;
}

export interface IItemData {
    items: IItem[];
}

export class Item {
    private _title: string;
    private _description: string;
    private _price: number;
    private _email: string;
    private _image: string;
    private _isfav = false;

    constructor(item: IItem) {
        this.title = item.title;
        this.description = item.description;
        this.price = item.price;
        this.email = item.email;
        this.image = item.image;
    }
    public get isfav(): boolean {
        return this._isfav;
    }
    public set isfav(value: boolean) {
        this._isfav = value;
    }

    public get title(): string {
        return this._title;
    }
    public set title(value: string) {
        this._title = value;
    }
    public get price(): number {
        return this._price;
    }
    public set price(value: number) {
        this._price = value;
    }
    public get email(): string {
        return this._email;
    }
    public set email(value: string) {
        this._email = value;
    }
    public get image(): string {
        return this._image;
    }
    public set image(value: string) {
        this._image = value;
    }
    public get description(): string {
        return this._description;
    }
    public set description(value: string) {
        this._description = value;
    }
}
