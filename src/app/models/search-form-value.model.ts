export interface SearchFormValue {
    title: string;
    description: string;
    price: string;
    email: string;
}
